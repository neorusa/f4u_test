# Author: Ruslan Gareev (mail@ruslangareev.ru)
from datetime import timedelta
from decimal import Decimal

from django.test import TestCase
from django.utils.datetime_safe import datetime

from account.models import Account
from customer.models import Customer
from scheduled_payment.models import ScheduledPayment
from transfer.models import Transfer


class ScheduledPaymentsTestCase(TestCase):
    def setUp(self) -> None:
        super(ScheduledPaymentsTestCase, self).setUp()
        customer = Customer.objects.create(
            email='test@test.invalid',
            full_name='Test Customer',
        )

        self.account1 = Account.objects.create(number=123, owner=customer, balance=1000)
        self.account2 = Account.objects.create(number=456, owner=customer, balance=1000)
        self.today = datetime.today()
        self.account1_scheduled_payment = ScheduledPayment.objects.create(
            account=self.account1,
            to_account=self.account2,
            start=self.today,
            end=self.today + timedelta(days=5),
            day_scheduled=self.today.day,
            amount=Decimal(100)
        )

    def test_get_scheduled_payments(self):
        sched_payments = ScheduledPayment.check_scheduled_payments_due()
        self.assertEqual(len(sched_payments), 1)

    def test_get_scheduled_payments_for_custom_user(self):
        sched_payments = ScheduledPayment.check_scheduled_payments_due(client_id=self.account1.id)
        self.assertEqual(len(sched_payments), 1)

    def test_create_transaction_for_scheduled_payment(self):
        self.account1_scheduled_payment.create_transaction_for_payment()
        self.assertTrue(Transfer.objects.filter(
            from_account=self.account1,
            to_account=self.account2,
            amount=100,
        ).exists())
