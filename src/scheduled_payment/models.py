from typing import Optional

from django.db import models
from django.utils.datetime_safe import datetime

from transfer.models import Transfer


class ScheduledPayment(models.Model):
    account = models.ForeignKey('account.Account', on_delete=models.CASCADE, related_name='from_account')
    to_account = models.ForeignKey('account.Account', on_delete=models.CASCADE, related_name='to_account')
    start = models.DateField()
    end = models.DateField()
    day_scheduled = models.IntegerField()
    amount = models.DecimalField(max_digits=18, decimal_places=2)

    @staticmethod
    def check_scheduled_payments_due(client_id: Optional[int] = None):
        today = datetime.now()
        today_day = today.day
        today_scheduled_payments = ScheduledPayment.objects.filter(
            start__lte=today,
            end__gte=today,
            day_scheduled=today_day,
        )
        if client_id:
            today_scheduled_payments = today_scheduled_payments.filter(account_id=client_id)
        return today_scheduled_payments

    def create_transaction_for_payment(self):
        Transfer.do_transfer(self.account, self.to_account, self.amount)
