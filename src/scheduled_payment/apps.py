from django.apps import AppConfig


class ScheduledPayment(AppConfig):
    name = 'scheduled_payment'
