# Author: Ruslan Gareev (mail@ruslangareev.ru)
from enum import Enum


class BaseTypes(Enum):
    @classmethod
    def CHOISES(cls):
        return tuple((i.value, i.value) for i in cls)


class JobStatus(BaseTypes):
    NEW = 'new'
    PROGRESS = 'progress'
    DONE = 'done'
    FAIL = 'fail'
    ERROR = 'error'
